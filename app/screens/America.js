import { StyleSheet, Text, View, FlatList } from 'react-native'
import React from 'react'
import axios from 'axios';
import { data, baseUrl, apiKey } from '../endpoint/Endpoints';
import Recipecard from '../components/Recipecard';
import { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable'
import Recipe from '../screens/Recipe';
import 'react-native-gesture-handler';

const Stack = createStackNavigator();

const Recipelist = ({ navigation }) => {
    const [cuisine, setCuisine] = React.useState([]);
    const onPressHandler = (recipe) => {
        navigation.navigate('recipe',
            { recipe });
        console.log('data');
    }
    useEffect(() => {
        getCuisine();
    }, [])

    const getCuisine = async () => {
        return axios({
            method: 'get',
            url: `${baseUrl}/recipes/complexSearch?apiKey=${apiKey}&cuisine=american`,
        }).then((response) => {
            setCuisine(response.data.results);
        })
    }

    return (
        <View style={{ backgroundColor: '#fff', flex: 1, alignItems: 'center' }}>
            {cuisine ? (<FlatList
                data={cuisine}
                renderItem={(recipe) => {
                    return (<View><Pressable onPress={() => onPressHandler(recipe.item)} >
                        <Recipecard image={recipe.item.image} title={recipe.item.title} />
                    </Pressable>
                    </View>)
                }} />) : null}


        </View>
    )
}
const America = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }}>
            <Stack.Screen name="recipelist" component={Recipelist} />
            <Stack.Screen name="recipe" component={Recipe} />
        </Stack.Navigator>

    )
}

export default America

const styles = StyleSheet.create({})