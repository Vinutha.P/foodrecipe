import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import axios from 'axios'
import { baseUrl, apiKey } from '../endpoint/Endpoints'
import Recipecard from '../components/Recipecard'
import { createStackNavigator } from '@react-navigation/stack';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable'
import Recipe from '../screens/Recipe';
import 'react-native-gesture-handler';
const Stack = createStackNavigator();

const Recipelist = ({ navigation }) => {
    const [Popular, setPopular] = React.useState();

    const onPressHandler = (recipe) => {
        navigation.navigate('recipe',
            { recipe });
    }

    useEffect(() => {
        getPopular();
    }, [])

    const getPopular = () => {
        axios({
            method: 'get',
            url: `${baseUrl}/recipes/random?apiKey=${apiKey}&number=9`,
        }).then((response) => {
            // console.log(response.data)
            setPopular(response.data);
        })
    }

    return (

        <View style={{ backgroundColor: '#fff', flex: 1 }}>
            <Text style={styles.text}>POPULAR PICKS</Text>
            <View style={{ alignItems: 'center' }}>
                {
                    Popular ? (<>
                        <ScrollView horizontal>
                            {/* {console.log(Popular)} */}
                            {Popular.recipes.map((recipe, index) => {
                                return (<Pressable onPress={() => onPressHandler(recipe)} key={index}>

                                    <Recipecard title={recipe.title} image={recipe.image} />

                                </Pressable>
                                )
                            })}
                        </ScrollView>
                        <Text style={styles.text}>VEGETARIAN PICKS</Text>
                        <ScrollView horizontal>
                            {Popular.recipes.map((recipe, index) => {
                                return (<Pressable onPress={() => onPressHandler(recipe)} key={index}>
                                    {recipe.vegetarian == true ? <Recipecard title={recipe.title} image={recipe.image} /> : null}

                                </Pressable>
                                )
                            })}
                        </ScrollView></>) : null
                }

            </View>
        </View>

    )

}

const Home = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }}>
            <Stack.Screen name="recipelist" component={Recipelist} />
            <Stack.Screen name="recipe" component={Recipe} />
        </Stack.Navigator>

    )
}

export default Home

const styles = StyleSheet.create({
    text: {
        fontSize: 15,
        color: 'purple',
        fontStyle: 'times new roman',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 20,
        marginTop: 20,
    },

})