import { StyleSheet, Text, View, Image, ScrollView } from 'react-native'
import React from 'react'
import { useState, useEffect } from 'react';
import axios from 'axios';
import { baseUrl, apiKey } from '../endpoint/Endpoints';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
import Instructionscard from '../components/Instructionscard';
import Ingrediantscard from '../components/Ingrediantscard';

const Recipe = ({ route }) => {
    const { recipe } = route.params;
    const [instructions, setInstructions] = useState(true);
    const [ingrediants, setIngrediants] = useState(false);
    const [cuisine, setCuisine] = useState({});

    const toggleIngrediants = () => {
        setIngrediants(!ingrediants);
        setInstructions(false);
    }

    const toggleInstructions = () => {
        setInstructions(!instructions);
        setIngrediants(false);
    }
    useEffect(() => {
        axios({
            method: 'get',
            url: `${baseUrl}/recipes/${recipe.id}/information?apiKey=${apiKey}`,
        }).then((response) => {
            setCuisine(response.data);
        }).catch((e) => console.log(e)
        )
    }, [])


    const checkIsNullOrEmptyObj = (val) => {
        if (val === null) {
            return null;
        } else if (val === undefined) {
            return null;
        } else if (!(Object.keys(val).length)) {
            return null;
        } else {
            return val;
        }
    }

    return (
        <ScrollView>
            <Text style={styles.text1}>{recipe.title}</Text>
            <Image style={styles.img} source={{ uri: recipe.image }} />

            <View style={styles.button}>

                <Pressable
                    style={[styles.press, instructions ? styles.pressactive : null,]} onPress={toggleInstructions} disabled={instructions}>
                    <Text style={[styles.presstext, instructions ? styles.presstextactive : null,]}>Instructions</Text>
                </Pressable>

                <Pressable style={[styles.press, ingrediants ? styles.pressactive : null,]} onPress={toggleIngrediants} disabled={ingrediants} >
                    <Text style={[styles.presstext, ingrediants ? styles.presstextactive : null,]}>Ingrediants</Text>
                </Pressable>


            </View>
            <View>

                {instructions &&
                    (checkIsNullOrEmptyObj(cuisine)) ?
                    (cuisine.analyzedInstructions[0].steps.map((steps) => {
                        return (
                            <Instructionscard step={steps.step} number={steps.number} />)
                    }))
                    : null
                }
                {ingrediants &&
                    (checkIsNullOrEmptyObj(cuisine)) ?
                    (cuisine.extendedIngredients.map((steps) => {
                        return (
                            <Ingrediantscard name={steps.name} amount={steps.amount} unit={steps.unit} />)
                    }))
                    : null}

            </View>
        </ScrollView>)
}

export default Recipe

const styles = StyleSheet.create({
    img: {
        height: 250,
        width: 250,
        resizeMode: "cover",
        borderRadius: 20,
        alignSelf: 'center',

    },
    text1: {
        fontSize: 18,
        fontWeight: 'bold',
        fontStyle: 'times new roman',
        color: 'purple',
        alignSelf: 'center',
        marginVertical: 10,

    },

    presstext: {
        fontSize: 20,
        fontWeight: 'bold',
        fontStyle: 'times new roman',
        color: 'black',
        alignSelf: 'center',
        marginVertical: 10,

    },
    presstextactive: {
        color: 'white',
    },
    press: {
        backgroundColor: 'white',
        borderColor: "black",
        width: 170,
        height: 50,
        marginVertical: 20,
        alignItems: "center",
        borderRadius: 20,
    },

    pressactive: {
        backgroundColor: 'black',
    },
    button: {
        flexDirection: "row",
        justifyContent: "space-around",

    },


})