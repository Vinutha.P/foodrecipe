import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Ingrediantscard = (props) => {
    return (
        <View style={styles.instructContainer}>
            <Text style={styles.information}>{props.name} {props.amount} {props.unit}</Text>
        </View>
    )
}

export default Ingrediantscard

const styles = StyleSheet.create({
    instructContainer: {
        marginHorizontal: 10,
        marginVertical: 10,
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
    },
    information: {
        textAlign: 'justify',
        fontFamily: 'bold',
        fontStyle: 'times new roman',
        fontSize: 15,
        color: 'black',
    }
})