import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'



const Recipecard = (props) => {
    return (

        <View style={styles.imageContainer}>

            <Image style={styles.img} source={{ uri: props.image }} />
            <Text style={styles.text1}>{props.title}</Text>

        </View>


    )
}

export default Recipecard

const styles = StyleSheet.create({
    card: {
        width: "100%",
        height: "100%",
        padding: 5,
        flex: 1,


    },

    imageContainer: {
        alignItems: 'center',
        marginHorizontal: 10,
        width: 300,
        height: 250,
        marginVertical: 10,

        justifyContent: 'center',
    },
    img: {
        height: "100%",
        width: "100%",
        resizeMode: "cover",
        borderRadius: 20,

    },

    text1: {
        fontSize: 14,
        color: '#fff',
        fontWeight: 'bold',
        fontStyle: 'times new roman',
        position: 'absolute',
        bottom: 10,

    },
    container: {
        height: 450,

    }


})