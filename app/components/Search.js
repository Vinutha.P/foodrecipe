import { StyleSheet, Text, View, TextInput } from 'react-native'
import React from 'react'
import { useState } from 'react'
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


const Search = () => {
    const [text, setText] = useState('');

    const txtHandler = (e) => {
        setText(e);
    };

    return (
        <View>
            {/* <Icon style={styles.searchIcon} name="food" size={20} color="#000" /> */}

            <TextInput style={styles.text}
                placeholder="eg:Burger" value={text} onChangeText={txtHandler} />
        </View>
    )
}

export default Search

const styles = StyleSheet.create({
    text: {
        width: 360,
        alignSelf: "center",
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 17,
    },
    searchIcon: {
        padding: 10,
    },

})