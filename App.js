import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import America from './app/screens/America';
import Home from './app/screens/Home';
import Itali from './app/screens/Itali';
import Thai from './app/screens/Thai';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Search from './app/components/Search';
import Recipe from './app/screens/Recipe';
import { LogBox } from 'react-native';



LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
]);





const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <View style={{ flex: 1 }}>
      <View >
        <Text style={styles.text}>Delicious</Text>
        <Search />
      </View>
      <NavigationContainer>
        <Tab.Navigator screenOptions={{

          headerShown: false,
          tabBarLabelStyle: {
            fontSize: 15,
            fontStyle: 'Roboto',
          },
          tabBarActiveTintColor: 'purple',
        }}>

          <Tab.Screen
            name="Home" component={Home} options={{
              tabBarIcon: ({ focused }) => (
                <Icon name="food" size={26} color={focused ? "purple" : "grey"} />),
            }} />

          <Tab.Screen name="America" component={America} options={{
            tabBarIcon: ({ focused }) => (
              <Icon name="food-drumstick" size={26} color={focused ? "purple" : "grey"} />),
          }} />

          <Tab.Screen name="Italian" component={Itali} options={{
            tabBarIcon: ({ focused }) => (
              <Icon name="food-variant" size={26} color={focused ? "purple" : "grey"} />),
          }} />

          <Tab.Screen name="Thai" component={Thai} options={{
            tabBarIcon: ({ focused }) => (
              <Icon name="food-turkey" size={26} color={focused ? "purple" : "grey"} />),
          }} />
        </Tab.Navigator>
      </NavigationContainer>

    </View>


  )
}

export default App

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    color: 'purple',
    fontStyle: 'italic',
    fontWeight: 'bold',
    alignSelf: "center",

  }
})